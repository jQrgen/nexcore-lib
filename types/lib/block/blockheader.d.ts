export = BlockHeader;
/**
* @typedef {{
*  prevHash: Buffer;
*  bits: Buffer;
*  ancestorHash: Buffer;
*  merkleRoot: Buffer;
*  txFilter: Buffer;
*  time: number;
*  height: number;
*  chainWork: Buffer;
*  size: BN;
*  txCount: number;
*  poolFee: number;
*  utxoCommitment: Buffer;
*  minerData: Buffer;
*  nonce: Buffer;
* }} BlockHeaderProps
*/
/**
 * Instantiate a BlockHeader from a Buffer, JSON object, or Object with
 * the properties of the BlockHeader
 *
 * @param {Buffer|object} arg - A Buffer, JSON object, or Object
 * @returns {BlockHeader} - An instance of block header
 * @constructor
 */
declare function BlockHeader(arg: Buffer | object): BlockHeader;
declare class BlockHeader {
    /**
    * @typedef {{
    *  prevHash: Buffer;
    *  bits: Buffer;
    *  ancestorHash: Buffer;
    *  merkleRoot: Buffer;
    *  txFilter: Buffer;
    *  time: number;
    *  height: number;
    *  chainWork: Buffer;
    *  size: BN;
    *  txCount: number;
    *  poolFee: number;
    *  utxoCommitment: Buffer;
    *  minerData: Buffer;
    *  nonce: Buffer;
    * }} BlockHeaderProps
    */
    /**
     * Instantiate a BlockHeader from a Buffer, JSON object, or Object with
     * the properties of the BlockHeader
     *
     * @param {Buffer|object} arg - A Buffer, JSON object, or Object
     * @returns {BlockHeader} - An instance of block header
     * @constructor
     */
    constructor(arg: Buffer | object);
    prevHash: Buffer;
    bits: Buffer;
    ancestorHash: Buffer;
    merkleRoot: Buffer;
    txFilter: Buffer;
    time: number;
    height: number;
    chainWork: Buffer;
    size: BN;
    txCount: number;
    poolFee: number;
    utxoCommitment: Buffer;
    minerData: Buffer;
    nonce: Buffer;
    /**
     * @returns {Object} - A plain object of the BlockHeader
     */
    toObject: () => any;
    toJSON(): any;
    /**
     * @returns {Buffer} - A Buffer of the BlockHeader
     */
    toBuffer(): Buffer;
    /**
     * @returns {string} - A hex encoded string of the BlockHeader
     */
    toString(): string;
    /**
     * @param {BufferWriter} bw - An existing instance BufferWriter
     * @returns {BufferWriter} - An instance of BufferWriter representation of the BlockHeader
     */
    toBufferWriter(bw: BufferWriter): BufferWriter;
    /**
     * Returns the target difficulty for this block
     * @param {Number=} bits
     * @returns {BN} An instance of BN with the decoded difficulty bits
     */
    getTargetDifficulty(bits?: number | undefined): BN;
    /**
     * @return {Number}
     */
    getDifficulty(): number;
    /**
     * @returns {Buffer} - The little endian hash buffer of the header
     */
    _getHash(): Buffer;
    id: any;
    hash: any;
    /**
     * @returns {Boolean} - If timestamp is not too far in the future
     */
    validTimestamp(): boolean;
    /**
     * @returns {Boolean} - If the proof-of-work hash satisfies the target difficulty
     */
    validProofOfWork(): boolean;
    /**
     * @returns {string} - A string formatted for the console
     */
    inspect(): string;
}
declare namespace BlockHeader {
    export { _from, _fromObject, fromObject, fromRawBlock, fromBuffer, fromString, _fromBufferReader, fromBufferReader, Constants, BlockHeaderProps };
}
import BN = require("bn.js");
import BufferWriter = require("../encoding/bufferwriter");
/**
 * @param {Buffer|BlockHeaderProps|object} arg - A Buffer, JSON object or Object
 * @returns {BlockHeaderProps} - An object representing block header data
 * @throws {TypeError} - If the argument was not recognized
 * @private
 */
declare function _from(arg: Buffer | BlockHeaderProps | object): BlockHeaderProps;
/**
 * @param {object|BlockHeaderProps} data - A JSON object
 * @returns {BlockHeaderProps} - An object representing block header data
 * @private
 */
declare function _fromObject(data: object | BlockHeaderProps): BlockHeaderProps;
/**
 * @param {object} obj - A plain JavaScript object
 * @returns {BlockHeader} - An instance of block header
 */
declare function fromObject(obj: object): BlockHeader;
/**
 * @param {BinaryData|Buffer} data - Raw block binary data or buffer
 * @returns {BlockHeader} - An instance of block header
 */
declare function fromRawBlock(data: BinaryData | Buffer): BlockHeader;
/**
 * @param {Buffer} buf - A buffer of the block header
 * @returns {BlockHeader} - An instance of block header
 */
declare function fromBuffer(buf: Buffer): BlockHeader;
/**
 * @param {string} str - A hex encoded buffer of the block header
 * @returns {BlockHeader} - An instance of block header
 */
declare function fromString(str: string): BlockHeader;
/**
 * @param {BufferReader} br - A BufferReader of the block header
 * @returns {BlockHeaderProps} - An object representing block header data
 * @private
 */
declare function _fromBufferReader(br: BufferReader): BlockHeaderProps;
/**
 * @param {BufferReader} br - A BufferReader of the block header
 * @returns {BlockHeader} - An instance of block header
 */
declare function fromBufferReader(br: BufferReader): BlockHeader;
declare namespace Constants {
    let START_OF_HEADER: number;
    let MAX_TIME_OFFSET: number;
    let LARGEST_HASH: BN;
}
type BlockHeaderProps = {
    prevHash: Buffer;
    bits: Buffer;
    ancestorHash: Buffer;
    merkleRoot: Buffer;
    txFilter: Buffer;
    time: number;
    height: number;
    chainWork: Buffer;
    size: BN;
    txCount: number;
    poolFee: number;
    utxoCommitment: Buffer;
    minerData: Buffer;
    nonce: Buffer;
};
import BufferReader = require("../encoding/bufferreader");
//# sourceMappingURL=blockheader.d.ts.map
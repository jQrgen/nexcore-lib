/// <reference types="node" />
export = Input;
declare function Input(params: any): Input;
declare class Input {
    constructor(params: any);
    get script(): any;
    _fromObject(params: any): this;
    type: number;
    output: any;
    prevTxId: any;
    outputIndex: any;
    sequenceNumber: any;
    amount: any;
    amountBN: any;
    toObject: () => {
        type: number;
        prevTxId: any;
        outputIndex: any;
        sequenceNumber: any;
        script: string;
        amount: any;
    };
    toJSON(): {
        type: number;
        prevTxId: any;
        outputIndex: any;
        sequenceNumber: any;
        script: string;
        amount: any;
    };
    toBufferWriter(writer: any): any;
    toBufferWriterIdem(writer: any): any;
    setScript(script: any): this;
    _script: import("../../script/script");
    _scriptBuffer: buffer.Buffer;
    /**
     * Retrieve signatures for the provided PrivateKey.
     *
     * @param {Transaction} transaction - the transaction to be signed
     * @param {PrivateKey} privateKey - the private key to use when signing
     * @param {number} inputIndex - the index of this input in the provided transaction
     * @param {number} sigType - defaults to Signature.SIGHASH_ALL
     * @param {Buffer} addressHash - if provided, don't calculate the hash of the
     *     public key associated with the private key provided
     * @param {String} signingMethod "schnorr" or "ecdsa", default to "ecdsa" if not provided
     * @abstract
     */
    getSignatures(): never;
    isFullySigned(): never;
    isFinal(): boolean;
    addSignature(): never;
    clearSignatures(): never;
    isValidSignature(transaction: any, signature: any, signingMethod: any): boolean;
    /**
     * @returns true if this is a coinbase input (represents no input)
     */
    isNull(): boolean;
    _estimateSize(): any;
    /**
     * Sets sequence number so that transaction is not valid until the desired seconds
     *  since the transaction is mined
     *
     * @param {Number} time in seconds
     * @return {Transaction} this
     */
    lockForSeconds(seconds: any): Transaction;
    /**
     * Sets sequence number so that transaction is not valid until the desired block height differnece since the tx is mined
     *
     * @param {Number} height
     * @return {Transaction} this
     */
    lockUntilBlockHeight(heightDiff: any): Transaction;
    /**
     *  Returns a semantic version of the input's sequence nLockTime.
     *  @return {Number|Date}
     *  If sequence lock is disabled  it returns null,
     *  if is set to block height lock, returns a block height (number)
     *  else it returns a Date object.
     */
    getLockTime(): number | Date;
}
declare namespace Input {
    export { MAXINT };
    export { DEFAULT_SEQNUMBER };
    export { DEFAULT_LOCKTIME_SEQNUMBER };
    export { DEFAULT_RBF_SEQNUMBER };
    export { SEQUENCE_LOCKTIME_TYPE_FLAG };
    export { DEFAULT_TYPE };
    export function fromObject(obj: any): Input;
    export function fromBufferReader(br: any): Input;
}
import buffer = require("buffer");
declare var MAXINT: number;
declare var DEFAULT_SEQNUMBER: number;
declare var DEFAULT_LOCKTIME_SEQNUMBER: number;
declare var DEFAULT_RBF_SEQNUMBER: number;
declare const SEQUENCE_LOCKTIME_TYPE_FLAG: number;
declare const DEFAULT_TYPE: 0;
//# sourceMappingURL=input.d.ts.map
export function getMerkleRoot(hashes: any): any;
export function generateMerkleRootFromPublicKeys(publicKeys: any): any;
export function generateInputPublicKeyValidationOperations(inputPublicKeys: any): any[];
export function generateRedeemScriptOperations(inputPublicKeys: any, reclaimPublicKey: any): any[];
//# sourceMappingURL=escrow.d.ts.map
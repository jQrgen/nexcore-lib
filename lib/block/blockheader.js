'use strict';

var _ = require('lodash');
var BN = require('../crypto/bn');
var BufferUtil = require('../util/buffer');
var BufferReader = require('../encoding/bufferreader');
var BufferWriter = require('../encoding/bufferwriter');
var Hash = require('../crypto/hash');
var $ = require('../util/preconditions');

/**
* @typedef {{ 
*  prevHash: Buffer;
*  bits: Buffer;
*  ancestorHash: Buffer;
*  merkleRoot: Buffer;
*  txFilter: Buffer;
*  time: number;
*  height: number;
*  chainWork: Buffer;
*  size: BN;
*  txCount: number;
*  poolFee: number;
*  utxoCommitment: Buffer;
*  minerData: Buffer;
*  nonce: Buffer;
* }} BlockHeaderProps
*/

/**
 * Instantiate a BlockHeader from a Buffer, JSON object, or Object with
 * the properties of the BlockHeader
 *
 * @param {Buffer|object} arg - A Buffer, JSON object, or Object
 * @returns {BlockHeader} - An instance of block header
 * @constructor
 */
function BlockHeader(arg) {
  if (!(this instanceof BlockHeader)) {
    return new BlockHeader(arg);
  }
  var info = BlockHeader._from(arg);
  
  this.prevHash = info.prevHash,
  this.bits = info.bits,
  this.ancestorHash = info.ancestorHash,
  this.merkleRoot = info.merkleRoot,
  this.txFilter = info.txFilter,
  this.time = info.time,
  this.height = info.height,
  this.chainWork = info.chainWork,
  this.size = info.size,
  this.txCount = info.txCount,
  this.poolFee = info.poolFee,
  this.utxoCommitment = info.utxoCommitment,
  this.minerData = info.minerData,
  this.nonce = info.nonce

  if (info.hash) {
    $.checkState(
      this.hash === info.hash,
      'Argument object hash property does not match block hash.'
    );
  }

  return this;
};

/**
 * @param {Buffer|BlockHeaderProps|object} arg - A Buffer, JSON object or Object
 * @returns {BlockHeaderProps} - An object representing block header data
 * @throws {TypeError} - If the argument was not recognized
 * @private
 */
BlockHeader._from = function _from(arg) {
  var info = {};
  if (BufferUtil.isBuffer(arg)) {
    info = BlockHeader._fromBufferReader(BufferReader(arg));
  } else if (_.isObject(arg)) {
    info = BlockHeader._fromObject(arg);
  } else {
    throw new TypeError('Unrecognized argument for BlockHeader');
  }
  return info;
};

/**
 * @param {object|BlockHeaderProps} data - A JSON object
 * @returns {BlockHeaderProps} - An object representing block header data
 * @private
 */
BlockHeader._fromObject = function _fromObject(data) {
  $.checkArgument(data, 'data is required');
  var prevHash = data.prevHash;
  var bits = data.bits;
  var ancestorHash = data.ancestorHash;
  var merkleRoot = data.merkleRoot;
  var txFilter = data.txFilter;
  var chainWork = data.chainWork;
  var size = data.size;
  var utxoCommitment = data.utxoCommitment;
  var minerData = data.minerData;
  var nonce = data.nonce;
  if (_.isString(data.prevHash)) {
    prevHash = Buffer.from(data.prevHash, 'hex').reverse();
  }
  if (_.isString(data.bits)) {
    bits = Buffer.from(data.bits, 'hex').reverse();
  }
  if (_.isString(data.ancestorHash)) {
    ancestorHash = Buffer.from(data.ancestorHash, 'hex').reverse();
  }
  if (_.isString(data.merkleRoot)) {
    merkleRoot = Buffer.from(data.merkleRoot, 'hex').reverse();
  }
  if (_.isString(data.txFilter)) {
    txFilter = Buffer.from(data.txFilter, 'hex').reverse();
  }
  if (_.isString(data.chainWork)) {
    chainWork = Buffer.from(data.chainWork, 'hex').reverse();
  }
  if (_.isNumber(data.size)) {
    size = BN.fromNumber(data.size);
  }
  if (_.isString(data.utxoCommitment)) {
    utxoCommitment = Buffer.from(data.utxoCommitment, 'hex');
  }
  if (_.isString(data.minerData)) {
    minerData = Buffer.from(data.minerData, 'hex');
  }
  if (_.isString(data.nonce)) {
    nonce = Buffer.from(data.nonce, 'hex');
  }

  var info = {
    prevHash: prevHash,
    bits: bits,
    ancestorHash: ancestorHash,
    merkleRoot: merkleRoot,
    txFilter: txFilter,
    time: data.time,
    height: data.height,
    chainWork: chainWork,
    size: size,
    txCount: data.txCount,
    poolFee: data.poolFee,
    utxoCommitment: utxoCommitment,
    minerData: minerData,
    nonce: nonce
  };

  return info;
};

/**
 * @param {object} obj - A plain JavaScript object
 * @returns {BlockHeader} - An instance of block header
 */
BlockHeader.fromObject = function fromObject(obj) {
  var info = BlockHeader._fromObject(obj);
  return new BlockHeader(info);
};

/**
 * @param {BinaryData|Buffer} data - Raw block binary data or buffer
 * @returns {BlockHeader} - An instance of block header
 */
BlockHeader.fromRawBlock = function fromRawBlock(data) {
  if (!BufferUtil.isBuffer(data)) {
    data = Buffer.from(data, 'binary');
  }
  var br = BufferReader(data);
  br.pos = BlockHeader.Constants.START_OF_HEADER;
  var info = BlockHeader._fromBufferReader(br);
  return new BlockHeader(info);
};

/**
 * @param {Buffer} buf - A buffer of the block header
 * @returns {BlockHeader} - An instance of block header
 */
BlockHeader.fromBuffer = function fromBuffer(buf) {
  var info = BlockHeader._fromBufferReader(BufferReader(buf));
  return new BlockHeader(info);
};

/**
 * @param {string} str - A hex encoded buffer of the block header
 * @returns {BlockHeader} - An instance of block header
 */
BlockHeader.fromString = function fromString(str) {
  var buf = Buffer.from(str, 'hex');
  return BlockHeader.fromBuffer(buf);
};

/**
 * @param {BufferReader} br - A BufferReader of the block header
 * @returns {BlockHeaderProps} - An object representing block header data
 * @private
 */
BlockHeader._fromBufferReader = function _fromBufferReader(br) {
  var info = {};
  info.prevHash = br.read(32);
  info.bits = br.read(4);
  info.ancestorHash = br.read(32);
  info.merkleRoot = br.read(32);
  info.txFilter = br.read(32);
  info.time = br.readUInt32LE();
  info.height = br.readCoreVarintNum();
  info.chainWork = br.read(32);
  info.size = br.readUInt64LEBN();
  info.txCount = br.readCoreVarintNum();
  info.poolFee = br.readCoreVarintNum();
  info.utxoCommitment = br.readVarLengthBuffer();
  info.minerData = br.readVarLengthBuffer();
  info.nonce = br.readVarLengthBuffer();
  return info;
};

/**
 * @param {BufferReader} br - A BufferReader of the block header
 * @returns {BlockHeader} - An instance of block header
 */
BlockHeader.fromBufferReader = function fromBufferReader(br) {
  var info = BlockHeader._fromBufferReader(br);
  return new BlockHeader(info);
};

/**
 * @returns {Object} - A plain object of the BlockHeader
 */
BlockHeader.prototype.toObject = BlockHeader.prototype.toJSON = function toObject() {
  return {
    hash: this.hash,
    prevHash: BufferUtil.reverse(this.prevHash).toString('hex'),
    bits: BufferUtil.reverse(this.bits).toString('hex'),
    ancestorHash: BufferUtil.reverse(this.ancestorHash).toString('hex'),
    merkleRoot: BufferUtil.reverse(this.merkleRoot).toString('hex'),
    txFilter: BufferUtil.reverse(this.txFilter).toString('hex'),
    time: this.time,
    height: this.height,
    chainWork: BufferUtil.reverse(this.chainWork).toString('hex'),
    size: this.size.toNumber(),
    txCount: this.txCount,
    poolFee: this.poolFee,
    utxoCommitment: this.utxoCommitment.toString('hex'),
    minerData: this.minerData.toString('hex'),
    nonce: this.nonce.toString('hex'),
  };
};

/**
 * @returns {Buffer} - A Buffer of the BlockHeader
 */
BlockHeader.prototype.toBuffer = function toBuffer() {
  return this.toBufferWriter().concat();
};

/**
 * @returns {string} - A hex encoded string of the BlockHeader
 */
BlockHeader.prototype.toString = function toString() {
  return this.toBuffer().toString('hex');
};

/**
 * @param {BufferWriter} bw - An existing instance BufferWriter
 * @returns {BufferWriter} - An instance of BufferWriter representation of the BlockHeader
 */
BlockHeader.prototype.toBufferWriter = function toBufferWriter(bw) {
  if (!bw) {
    bw = new BufferWriter();
  }
  bw.write(this.prevHash);
  bw.write(this.bits);
  bw.write(this.ancestorHash);
  bw.write(this.merkleRoot);
  bw.write(this.txFilter);
  bw.writeInt32LE(this.time);
  bw.writeCoreVarintNum(this.height);
  bw.write(this.chainWork);
  bw.writeUInt64LEBN(this.size);
  bw.writeCoreVarintNum(this.txCount);
  bw.writeCoreVarintNum(this.poolFee);
  bw.writeVarLengthBuf(this.utxoCommitment);
  bw.writeVarLengthBuf(this.minerData);
  bw.writeVarLengthBuf(this.nonce);

  return bw;
};

/**
 * Returns the target difficulty for this block
 * @param {Number=} bits
 * @returns {BN} An instance of BN with the decoded difficulty bits
 */
BlockHeader.prototype.getTargetDifficulty = function getTargetDifficulty(bits) {
  bits = bits || this.bits;

  var exponent = BN.fromBuffer(Buffer.from([bits[0]]));
  var significand = BN.fromBuffer(bits.slice(1));

  var target = significand.toBigInt() * (2n ** (8n * (exponent.toBigInt() - 3n)));
  
  return new BN(target.toString());
};

/**
 * @return {Number}
 */
BlockHeader.prototype.getDifficulty = function getDifficulty() {
  var bits = BN.fromBuffer(this.bits).toNumber();
  var nShift = (bits >> 24) & 0xff;

  var dDiff = 0x0000ffff / (bits & 0x00ffffff);

  while (nShift < 29)
  {
      dDiff *= 256.0;
      nShift++;
  }
  while (nShift > 29)
  {
      dDiff /= 256.0;
      nShift--;
  }

  return dDiff;
};

/**
 * @returns {Buffer} - The little endian hash buffer of the header
 */
BlockHeader.prototype._getHash = function hash() {
  var miniHeader = new BufferWriter();
  miniHeader.write(this.prevHash);
  miniHeader.write(this.bits);

  var miniHash = Hash.sha256(miniHeader.toBuffer())

  var extHeader = new BufferWriter();
  extHeader.write(this.ancestorHash);
  extHeader.write(this.txFilter);
  extHeader.write(this.merkleRoot);
  extHeader.writeInt32LE(this.time);
  extHeader.writeUInt64LEBN(BN.fromNumber(this.height));
  extHeader.write(this.chainWork);
  extHeader.writeUInt64LEBN(this.size);
  extHeader.writeUInt64LEBN(BN.fromNumber(this.txCount));
  extHeader.writeUInt64LEBN(BN.fromNumber(this.poolFee));
  extHeader.writeVarLengthBuf(this.utxoCommitment);
  extHeader.writeVarLengthBuf(this.minerData);
  extHeader.writeVarLengthBuf(this.nonce);

  var extHash = Hash.sha256(extHeader.toBuffer());

  var commintment = new BufferWriter();
  commintment.write(miniHash);
  commintment.write(extHash);

  return Hash.sha256(commintment.toBuffer());
};

var idProperty = {
  configurable: false,
  enumerable: true,
  /**
   * @returns {string} - The little endian hash buffer of the header
   */
  get: function() {
    if (!this._id) {
      this._id = BufferReader(this._getHash()).readReverse().toString('hex');
    }
    return this._id;
  },
  set: _.noop
};
Object.defineProperty(BlockHeader.prototype, 'id', idProperty);
Object.defineProperty(BlockHeader.prototype, 'hash', idProperty);

/**
 * @returns {Boolean} - If timestamp is not too far in the future
 */
BlockHeader.prototype.validTimestamp = function validTimestamp() {
  var currentTime = Math.round(new Date().getTime() / 1000);
  if (this.time > currentTime + BlockHeader.Constants.MAX_TIME_OFFSET) {
    return false;
  }
  return true;
};

/**
 * @returns {Boolean} - If the proof-of-work hash satisfies the target difficulty
 */
BlockHeader.prototype.validProofOfWork = function validProofOfWork() {
  var pow = new BN(this.id, 'hex');
  var target = this.getTargetDifficulty();

  if (pow.cmp(target) > 0) {
    return false;
  }
  return true;
};

/**
 * @returns {string} - A string formatted for the console
 */
BlockHeader.prototype.inspect = function inspect() {
  return '<BlockHeader ' + this.id + '>';
};

BlockHeader.Constants = {
  START_OF_HEADER: 8, // Start buffer position in raw block data
  MAX_TIME_OFFSET: 2 * 60 * 60, // The max a timestamp can be in the future
  LARGEST_HASH: new BN('10000000000000000000000000000000000000000000000000000000000000000', 'hex')
};

module.exports = BlockHeader;

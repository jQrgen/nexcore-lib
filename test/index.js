"use strict";

var should = require('chai').should();
var nexcore = require('../');

describe('#versionGuard', function() {
  it('global._nexcore should be defined', function() {
    should.equal(global._nexcore, nexcore.version);
  });

  it('throw an error if version is already defined', function() {
    (function() {
      nexcore.versionGuard('version');
    }).should.throw('More than one instance of nexcore');
  });
});

const nexcore = require('../index')

const main = function()
{

    console.log(nexcore.GroupToken.isAuthority(-4323455642275676160n))
    console.log(nexcore.GroupToken.allowsMint(-4323455642275676160n))
    console.log(nexcore.GroupToken.allowsMelt(-4323455642275676160n))
    console.log(nexcore.GroupToken.allowsRenew(-4323455642275676160n))
    console.log(nexcore.GroupToken.allowsRescript(-4323455642275676160n))
    console.log(nexcore.GroupToken.allowsSubgroup(-4323455642275676160n))
    console.log(nexcore.GroupToken.isGroupCreation(nexcore.Address.decodeNexaAddress('nexa:tr9v70v4s9s6jfwz32ts60zqmmkp50lqv7t0ux620d50xa7dhyqqqcg6kdm6f').getHashBuffer(), -4323455642275676160n))
    console.log(nexcore.GroupToken.isGroupCreation(nexcore.Address.decodeNexaAddress('nexa:tr9v70v4s9s6jfwz32ts60zqmmkp50lqv7t0ux620d50xa7dhyqqqcg6kdm6f').getHashBuffer(), -288230376151662285n))

    var groupAmount = -288230376151662285n;

    var amountBuf = nexcore.GroupToken.getAmountBuffer(groupAmount);
    console.log(amountBuf); //<Buffer 33 c1 00 00 00 00 00 fc>

    var signedAmount = nexcore.GroupToken.getAmountValue(amountBuf);
    var unsignedAmount = nexcore.GroupToken.getAmountValue(amountBuf, true);
    console.log(signedAmount) // -288230376151662285n
    console.log(unsignedAmount) // 18158513697557889331n
    console.log(signedAmount === groupAmount); // true


    amountBuf = Buffer.from('5d560000000000fc', 'hex');
    signedAmount = nexcore.GroupToken.getAmountValue(amountBuf);
    unsignedAmount = nexcore.GroupToken.getAmountValue(amountBuf, true);
    console.log(signedAmount); // -288230376151689635n
    console.log(unsignedAmount); // 18158513697557861981n

    var buf = nexcore.GroupToken.getAmountBuffer(signedAmount);
    var buf2 = nexcore.GroupToken.getAmountBuffer(unsignedAmount);
    console.log(buf.equals(buf2)) // true
    console.log(buf.equals(amountBuf)) // true
};

main();
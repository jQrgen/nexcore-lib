const nexcore = require('../index')

const main = function()
{
    var nexUnit = nexcore.Unit.fromNEX(50000000);

    console.log(nexUnit.toSatoshis());
    console.log(nexUnit.toKEX());
    console.log(nexUnit.toMEX());
    console.log(nexUnit.atRate(0.000005733));

    var dollarUnit = nexcore.Unit.fromFiat(200, 0.000005733); // 200$

    console.log(dollarUnit.toSatoshis());
    console.log(dollarUnit.toNEXA());
    console.log(dollarUnit.toKEX());
    console.log(dollarUnit.toMEX());
    console.log(dollarUnit.toJSON());
};

main();